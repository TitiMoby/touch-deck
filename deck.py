import board
import busio
import displayio
import json
import adafruit_touchscreen
from adafruit_bitmap_font import bitmap_font
from adafruit_display_text.label import Label
from adafruit_button import Button
import deck_configuration
import adafruit_requests as requests
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
from adafruit_esp32spi import adafruit_esp32spi
from digitalio import DigitalInOut

GREEN = 0x03AD31
WHITE = 0xFFFFFF
GRAY = GRAY = 0x777777

try:
    from secrets import secrets
except ImportError:
    print("Wifi secrets are kept in secrets.py")
    raise
esp32_cs = DigitalInOut(board.ESP_CS)
esp32_ready = DigitalInOut(board.ESP_BUSY)
esp32_reset = DigitalInOut(board.ESP_RESET)
spi = busio.SPI(board.SCK, board.MOSI, board.MISO)
esp = adafruit_esp32spi.ESP_SPIcontrol(spi, esp32_cs, esp32_ready, esp32_reset)

requests.set_socket(socket, esp)

if esp.status == adafruit_esp32spi.WL_IDLE_STATUS:
    print("ESP32 found and in idle mode")
print("Firmware vers.", esp.firmware_version)
print("MAC addr:", [hex(i) for i in esp.MAC_address])

print("Connecting to AP...")
while not esp.is_connected:
    try:
        esp.connect_AP(secrets["ssid"], secrets["password"])
    except RuntimeError as e:
        print("could not connect to AP, retrying: ", e)
        continue
print("Connected to", str(esp.ssid, "utf-8"), "\tRSSI:", esp.rssi)
print("My IP address is", esp.pretty_ip(esp.ip_address))

# Dictionnary containing methods that can be called from button actions
plugin_calls = {}
plugin_calls["nothing"]=None
# TODO: read all plugins found in dedicated folder to enrich plugin_calls
#elgato_light_url = "http://ekl1.local:9123/elgato/lights"
elgato_light_url = "http://10.0.4.41:9123/elgato/lights"

headers = {'Content-type': 'application/json'}
def light_on():
    print('call light_on')
    light_state(1)
def light_off():
    print('call light_off')
    light_state(0)
def light_state(state):
    payload = {
        "numberOfLights":1,
        "lights":[{
            "on": state,
            "brightness":24,
            "temperature":213
        }]
    }
    try:
        data = json.dumps(payload)
        print(data)
        response = requests.put(elgato_light_url, data=data, headers=headers)
    except Exception as e:
        print(e)
plugin_calls['light_on'] = light_on
plugin_calls['light_off'] = light_off
# end of TODO

config = deck_configuration.load_configuration('deck_config.json')
display = board.DISPLAY
# Touchscreen setup
screen_width = config['screen_width']
screen_height = config['screen_height']

ts = adafruit_touchscreen.Touchscreen(board.TOUCH_XL, board.TOUCH_XR,
                                      board.TOUCH_YD, board.TOUCH_YU,
                                      calibration=((5200, 59000), (5800, 57000)), size=(screen_width, screen_height))

main_DG = displayio.Group() # The Main Display Group
display.show(main_DG)

font = bitmap_font.load_font("/fonts/LeagueSpartan-Bold-16.bdf")
font.load_glyphs(b'abcdefghjiklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890- ()')

# store buttons in an array to find each one when touch detected
buttons = []
for idx in range(config['nb_buttons']):
    button_config = config['layout'][idx]
    button = Button(name = str(idx),
                    x = button_config['col_start'] * config['col_pixel_size'], 
                    y = button_config['row_start'] * config['row_pixel_size'],
                    width = config['col_pixel_size'] * button_config['col_size'],
                    height = config['row_pixel_size'] * button_config['row_size'],
                    label = button_config['name'],
                    label_font = font, 
                    label_color = GREEN,
                    fill_color = WHITE,
                    style=Button.ROUNDRECT)
    buttons.append(button)
    main_DG.append(button)

touched = -1
while True:
    touch = ts.touch_point
    if touch:
        for i, b in enumerate(buttons):
            if b.contains(touch):
                if touched != b.name:
                    print(b.label)
                    touched = b.name
                    plugin_calls[config['layout'][i]['action']]()
    else:
        touched = -1
