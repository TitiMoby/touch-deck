import json

def load_configuration(file_name):
    # load configuration file
    with open(file_name) as file:
        # convert json to python dict 
        deck_config = json.load(file)
        # convert string to int values where it makes sense
        deck_config['screen_width'] = int(deck_config['screen_width'])
        deck_config['screen_height'] = int(deck_config['screen_height'])
        deck_config['number_of_row'] = int(deck_config['number_of_row'])
        deck_config['number_of_cols'] = int(deck_config['number_of_cols'])
        deck_config['row_pixel_size'] = deck_config['screen_height'] // deck_config['number_of_row']
        deck_config['col_pixel_size'] = deck_config['screen_width'] // deck_config['number_of_cols']
        # this value is computed from the layout and stored
        nb_buttons = len(deck_config['layout'])
        deck_config['nb_buttons'] = nb_buttons
        for idx in range(nb_buttons):
            button = deck_config['layout'][idx]
            button['row_start'] = int(button['row_start'])
            button['col_start'] = int(button['col_start'])
            button['row_size'] = int(button['row_size'])
            button['col_size'] = int(button['col_size'])
    return deck_config
